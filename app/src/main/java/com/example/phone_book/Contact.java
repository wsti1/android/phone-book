package com.example.phone_book;

import androidx.annotation.NonNull;

public class Contact {

    private String id;
    private String name;
    private String number;

    Contact(String id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("{\"id\": %s,\"name\": \"%s\",\"number\": \"%s\"}", id, name, number);
    }
}
