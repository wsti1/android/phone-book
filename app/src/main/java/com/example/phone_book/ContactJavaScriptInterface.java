package com.example.phone_book;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.webkit.JavascriptInterface;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;

import static android.provider.ContactsContract.CommonDataKinds.Phone;

public class ContactJavaScriptInterface {

    private Context context;
    private ContentResolver contentResolver;

    public ContactJavaScriptInterface(Context context, ContentResolver contentResolver) {
        this.context = context;
        this.contentResolver = contentResolver;
    }

    @JavascriptInterface
    public String getContactsAsJson() {
        ArrayList<Contact> contacts = getContacts();
        return serializeToJsonArray(contacts);
    }

    ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<>();
        Cursor cursor = queryFacade(Contacts.CONTENT_URI, null, null);

        if ((cursor != null ? cursor.getCount() : 0) > 0) {
            while (cursor.moveToNext()) {
                String id = getValue(cursor, Contacts._ID);

                if (cursor.getInt(cursor.getColumnIndex(Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor parentCursor = queryFacade(Phone.CONTENT_URI, Phone.CONTACT_ID + " = ?", new String[]{id});

                    while (parentCursor.moveToNext()) {
                        String name = getValue(parentCursor, Contacts.DISPLAY_NAME);
                        String number = getValue(parentCursor, Phone.NUMBER);

                        contacts.add(new Contact(id, name, number));
                    }
                    parentCursor.close();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return contacts;
    }

    private Cursor queryFacade(Uri uri, String selection, String[] selectionArgs) {
        return contentResolver.query(uri, null, selection, selectionArgs, null);
    }

    private String getValue(Cursor cursor, String dataType) {
        return cursor.getString(cursor.getColumnIndex(dataType));
    }

    private String serializeToJsonArray(List<Contact> contacts) {
        StringBuilder builder = new StringBuilder("[");
        for (int i = 0; i < contacts.size(); i++) {
            builder.append(contacts.get(i).toString());
            if (i < contacts.size() - 1) {
                builder.append(",");
            }
        }
        builder.append("]");
        return builder.toString();
    }
}
